/**
 * @file
 **/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <float.h>

//==============================================================================
// Include the signatures of VALIB functions.
//==============================================================================
#include "valib.h"

//==============================================================================
// Include signatures of low-level VALIB functions locally.
// NOTE: This is not needed for the user-level functions of VALIB - their
// signatures are included in valib.h. This is needed only for testing the
// lower-level functions in this tester as well. A user of VALIB should not
// need access to these.
//==============================================================================
VA_REAL valib_max(VA_REAL a, VA_REAL b);
void valib_sym_antisym3(VA_REAL *A);
void valib_tatt3(VA_REAL *A, VA_REAL *T, VA_REAL *TATT);
void valib_constructQ3(VA_REAL sina, VA_REAL cosa, VA_REAL sinb, VA_REAL cosb,
                       VA_REAL sing, VA_REAL cosg, VA_REAL *Q);
void valib_constructQfromN3(VA_REAL *n, VA_REAL *Q);
void valib_gram_schmidt3(VA_REAL *Q);
void valib_eigenvalues_sym3(VA_REAL *A, VA_REAL *eigval);
void valib_second_invariant3(VA_REAL *A, VA_REAL *second_invariant);
void valib_matmat3_prod(int trans_a, int trans_b,
                        VA_REAL *A, VA_REAL *B, VA_REAL *C);
void valib_norm_frobenius3(VA_REAL *A, VA_REAL *norm);
void valib_mat_copy3(VA_REAL *A, VA_REAL *B);
void valib_mat_zero3(VA_REAL *A);
void valib_trace3(VA_REAL *A, VA_REAL *tr3);
void valib_second_invariant3(VA_REAL *A, VA_REAL *second_invariant);
void valib_determinant3(VA_REAL *A, VA_REAL *det);
void valib_solve_cubic_equation(VA_REAL a, VA_REAL b, VA_REAL c, VA_REAL d,
                                VA_COMPLEX *x1, VA_COMPLEX *x2, VA_COMPLEX *x3,
                                int *info);

//==============================================================================
// Coefficient for conversion of degrees to radians.
static const VA_REAL DEG2RAD = 0.017453293;
//==============================================================================

//==============================================================================
// Convenience functions for printing scalars, vectors, and matrices.
//==============================================================================
static void print_scalar(char *name, VA_REAL alpha) {
    printf("  %s = %13.6f \n", name, alpha);
}

static void print_complex_scalar(char *name, VA_COMPLEX alpha) {
    printf("  %s = %13.6f + %13.6fi \n", name, creal(alpha), cimag(alpha));
}

static void print_vector3(char *name, VA_REAL *vec) {
    printf("  %s = [ %12.7f, %13.6f, %13.6f ]\n", name,
                                                  vec[0], vec[1], vec[2]);
}

static void print_matrix3(char *name, VA_REAL *mat) {
    printf("  %s:\n", name);
    int i, j;
    for (i = 0; i < 3; i++) {
        printf("   [ ");
        for (j = 0; j < 3; j++) {
            printf(" %13.6f ", mat[j*3+i]);
        }
        printf(" ]\n");
    }
}

//==============================================================================
// Convenience functions for checking equivalence of scalars, vectors,
// and matrices.
//==============================================================================
static int check_output(VA_REAL error, VA_REAL multiple) {
    VA_REAL epsilon;
    if (sizeof(VA_REAL) == sizeof(float)) {
        epsilon = FLT_EPSILON;
    }
    else {
        epsilon = DBL_EPSILON;
    }

    VA_REAL limit = multiple * epsilon;
    if (error > limit) {
        printf("FAILED! error = %e > limit = %e \n", error, limit);
        return -1;
    }
    else {
        printf("Success. error = %e < limit = %e \n", error, limit);
        return 0;
    }
}

static int check_scalar(VA_REAL alpha1, VA_REAL alpha2) {
    VA_REAL diff = fabs(alpha2-alpha1);
    return check_output(diff, 10.*valib_max(fabs(alpha1), fabs(alpha2)));
}

//static int check_vector(VA_REAL *vec1, VA_REAL *vec2) {
//    VA_REAL diff[3];
//    diff[0] = vec2[0] - vec1[0];
//    diff[1] = vec2[1] - vec1[1];
//    diff[2] = vec2[2] - vec1[2];
//    VA_REAL norm_diff = sqrt(  diff[0]*diff[0]
//                             + diff[1]*diff[1]
//                             + diff[2]*diff[2]);
//    return check_output(norm_diff, 10.);
//}

static int check_matrix(VA_REAL *mat1, VA_REAL *mat2) {
    VA_REAL diff[3*3];
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            diff[j*3+i] = mat2[j*3+i] - mat1[j*3+i];
        }
    }
    VA_REAL norm_diff;
    valib_norm_frobenius3(diff, &norm_diff);
    VA_REAL norm1;
    valib_norm_frobenius3(mat1, &norm1);
    VA_REAL norm2;
    valib_norm_frobenius3(mat2, &norm2);

    return check_output(norm_diff, 10.*valib_max(fabs(norm1), fabs(norm2)));
}

//==============================================================================
// The main tester code.
//==============================================================================
int main(int argc, char *argv[])
{
    int i, j;

    //==========================================================================
    // Get a 3x3 matrix with a velocity gradient.
    // If the name of a file is given, read the 9 values of the gradient from
    // there. Otherwise, use the default values.
    //==========================================================================
    //==========================================================================
    printf("\n");
    printf("VALIB tester\n");
    printf("============\n");
    //==========================================================================
    VA_REAL A[3*3];
    if (argc > 1) {
        // name of a file with velocity gradient is given as a command line
        // argument
        if (argc > 2) {
            printf("Usage: %s [file_with_matrix.txt] [initial_rotation_alpha "
                   "initial_rotation_beta initial_rotation_gamma "
                   "number_of_steps_in_pi] \n", argv[0]);
            return -1;
        }

        char *fileName = argv[1];
        printf("File with a velocity gradient: %s", fileName);

        FILE *ifile = fopen(fileName, "r");
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                double aux;
                int ierr = fscanf(ifile, "%lf", &aux);
                assert(ierr == 1); // returns number of successfully read values
                A[j*3+i] = (VA_REAL) aux;
            }
        }
        fclose(ifile);
    }
    else {
        // file with the gradient not given, use the default velocity gradient
        A[0] =  1.; A[3] = -2.; A[6] =  3.;
        A[1] = -4.; A[4] =  5.; A[7] =  6.;
        A[2] = -7.; A[5] = -8.; A[8] = -6.;
    }
    print_matrix3("Matrix of the velocity gradient", A);

    //==========================================================================
    printf("\n");
    printf("Testing user-level functions of VALIB\n");
    printf("=====================================\n");
    //==========================================================================

    //==========================================================================
    printf("\n");
    printf("Average contrarotation:\n");
    printf("-----------------------\n");
    //==========================================================================
    VA_REAL contrarotation[3*3];
    valib_contrarotation(A, contrarotation);
    print_matrix3("average contrarotation tensor", contrarotation);

    VA_REAL contrarotation_norm;
    valib_norm_frobenius3(contrarotation, &contrarotation_norm);
    print_scalar("norm of average contrarotation", contrarotation_norm);

    //==========================================================================
    printf("\n");
    printf("Average corotation:\n");
    printf("-------------------\n");
    //==========================================================================
    VA_REAL vorticity[3];
    vorticity[0] = A[2+1*3] - A[1+2*3];
    vorticity[1] = A[0+2*3] - A[2+0*3];
    vorticity[2] = A[1+0*3] - A[0+1*3];
    print_vector3("vorticity vector for reference", vorticity);

    VA_REAL corotation[3];
    valib_corotation(A, corotation);
    print_vector3("average corotation vector     ", corotation);

    VA_REAL corotation_magnitude = sqrt(  corotation[0]*corotation[0]
                                        + corotation[1]*corotation[1]
                                        + corotation[2]*corotation[2]);
    print_scalar("magnitude of average corotation", corotation_magnitude);

    //==========================================================================
    printf("\n");
    printf("Delta-criterion:\n");
    printf("----------------\n");
    //==========================================================================
    VA_REAL delta;
    valib_delta(A, &delta);
    print_scalar("Delta-criterion value", delta);

    //==========================================================================
    printf("\n");
    printf("lambda_2-criterion:\n");
    printf("-------------------\n");
    //==========================================================================
    VA_REAL lambda_2;
    valib_lambda2(A, &lambda_2);
    print_scalar("lambda_2", lambda_2);

    //==========================================================================
    printf("\n");
    printf("Q-criterion:\n");
    printf("------------\n");
    //==========================================================================
    VA_REAL qcriterion;
    valib_qcriterion(A, &qcriterion);
    print_scalar("Q-criterion value      ", qcriterion);

    // check the value by my own computation
    VA_REAL S[3*3];
    VA_REAL Omega[3*3];
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            S[j*3+i]     = 0.5 * (A[j*3+i] + A[i*3+j]);
            Omega[j*3+i] = 0.5 * (A[j*3+i] - A[i*3+j]);
        }
    }
    VA_REAL norm_Omega, norm_S;
    valib_norm_frobenius3(Omega, &norm_Omega);
    valib_norm_frobenius3(S,     &norm_S);
    VA_REAL qCheck = 0.5 * (norm_Omega*norm_Omega - norm_S*norm_S);
    print_scalar("Q-criterion value check", qCheck);
    check_scalar(qcriterion, qCheck);

    //==========================================================================
    printf("\n");
    printf("Q_D-criterion:\n");
    printf("--------------\n");
    //==========================================================================
    VA_REAL qdcriterion;
    valib_qdcriterion(A, &qdcriterion);
    print_scalar("Q_D-criterion value      ", qdcriterion);

    // check the value by an an own computation
    VA_REAL traceA = A[0] + A[4] + A[8];
    VA_REAL qdCheck = qcriterion + 1./6. * traceA * traceA;
    print_scalar("Q_D-criterion value check", qdCheck);
    check_scalar(qdcriterion, qdCheck);

    //==========================================================================
    printf("\n");
    printf("Q_M-criterion:\n");
    printf("--------------\n");
    //==========================================================================
    VA_REAL qmcriterion;
    valib_qmcriterion(A, &qmcriterion);
    print_scalar("Q_M-criterion value      ", qmcriterion);

    // check the value by an own computation
    VA_REAL eigval[3];
    valib_eigenvalues_sym3(S, eigval);
    VA_REAL secondInv = eigval[0]*eigval[1]
                      + eigval[1]*eigval[2]
                      + eigval[2]*eigval[0];

    VA_REAL qmCheck = qcriterion + 0.5*secondInv;
    print_scalar("Q_M-criterion value check", qmCheck);
    check_scalar(qmcriterion, qmCheck);

    //==========================================================================
    printf("\n");
    printf("Q_W-criterion:\n");
    printf("--------------\n");
    //==========================================================================
    VA_REAL qwcriterion;
    valib_qwcriterion(A, &qwcriterion);
    print_scalar("Q_W-criterion value      ", qwcriterion);

    // check the value by an own computation
    VA_REAL qwCheck = 0.5 * (norm_Omega*norm_Omega - 1.2 * norm_S*norm_S);
    print_scalar("Q_W-criterion value check", qwCheck);
    check_scalar(qwcriterion, qwCheck);

    //==========================================================================
    printf("\n");
    printf("The Rortex vector:\n");
    printf("------------------\n");
    //==========================================================================
    VA_REAL rortex[3];
    valib_rortex(A, rortex);
    print_vector3("Rortex vector     ", rortex);

    VA_REAL rortex_magnitude = sqrt(  rortex[0]*rortex[0]
                                    + rortex[1]*rortex[1]
                                    + rortex[2]*rortex[2]);
    print_scalar("magnitude of Rortex", rortex_magnitude);

    //==========================================================================
    printf("\n");
    printf("Compactness:\n");
    printf("------------------\n");
    //==========================================================================
    VA_REAL compactness;
    valib_compactness(A, &compactness);
    print_scalar("value", compactness);

    //==========================================================================
    printf("\n");
    printf("Swirling strength criterion:\n");
    printf("----------------------------\n");
    //==========================================================================
    VA_REAL lambda_ci, lambda_cr;
    valib_swirling_strength(A, &lambda_ci, &lambda_cr);
    print_scalar("imaginary part of the complex eigenvalue", lambda_ci);
    print_scalar("real part of the complex eigenvalue", lambda_cr);
    print_scalar("ratio of re and im parts of complex eigenvalue",
                 lambda_cr/lambda_ci);

    //==========================================================================
    printf("\n");
    printf("Triple decomposition method:\n");
    printf("----------------------------\n");
    //==========================================================================
    // number of intervals along [0,180] degrees range of angles
    int num_intervals = 180;
    VA_REAL res_vort, res_strain, shear;
    valib_triple_decomposition(A, &num_intervals, &res_vort, &res_strain,
                               &shear);
    print_scalar("residual vorticity", res_vort);
    print_scalar("residual strain   ", res_strain);
    print_scalar("shear             ", shear);

    //==========================================================================
    printf("\n");
    printf("Triple decomposition method using 4 norms:\n");
    printf("------------------------------------------\n");
    //==========================================================================
    VA_REAL shear_vorticity, shear_strain;
    valib_triple_decomposition_4norms(A, &num_intervals, &res_vort, &res_strain,
                                      &shear_vorticity, &shear_strain);
    print_scalar("residual vorticity", res_vort);
    print_scalar("residual strain   ", res_strain);
    print_scalar("shear vorticity   ", shear_vorticity);
    print_scalar("shear strain      ", shear_strain);

    //==========================================================================
    printf("\n");
    printf("Triple decomposition method with angles:  \n");
    printf("------------------------------------------\n");
    //==========================================================================
    VA_REAL alpha_brf, beta_brf, gamma_brf;
    valib_triple_decomposition_with_angles(A, &num_intervals,
                                              &res_vort, &res_strain, &shear,
                                              &alpha_brf, &beta_brf, &gamma_brf);
    print_scalar("residual vorticity", res_vort);
    print_scalar("residual strain   ", res_strain);
    print_scalar("shear             ", shear);
    print_scalar("alpha             ", alpha_brf);
    print_scalar("beta              ", beta_brf);
    print_scalar("gamma             ", gamma_brf);

    //==========================================================================
    printf("\n\n");
    printf("Testing internal functions of VALIB\n");
    printf("===================================\n");
    //==========================================================================

    //==========================================================================
    printf("\n");
    printf("Constructing orthogonal matrix Q from three angles: \n");
    //==========================================================================
    // choose angles
    VA_REAL alpha    = 10.; // deg
    VA_REAL beta     = 20.; // deg
    VA_REAL gamma    = 30.; // deg
    print_scalar("  alpha = ", alpha);
    print_scalar("  beta  = ", beta);
    print_scalar("  gamma = ", gamma);

    VA_REAL cosa, sina, cosb, sinb, cosg, sing;
    cosa = cos(alpha * DEG2RAD);
    sina = sin(alpha * DEG2RAD);
    cosb = cos(beta  * DEG2RAD);
    sinb = sin(beta  * DEG2RAD);
    cosg = cos(gamma * DEG2RAD);
    sing = sin(gamma * DEG2RAD);

    VA_REAL Q[3*3];
    valib_constructQ3(sina, cosa, sinb, cosb, sing, cosg, Q);
    print_matrix3("Matrix Q", Q);

    //==========================================================================
    printf("\n");
    printf("Testing the matrix transforming function: \n");
    //==========================================================================
    VA_REAL QAQT[3*3];
    valib_tatt3(A, Q, QAQT);

    print_matrix3("Matrix A", A);
    print_matrix3("Matrix QAQ^T by library function", QAQT);

    VA_REAL aux1[3*3], aux2[3*3];
    for (int i = 0; i < 9; i++) {
        aux1[i] = 0.;
        aux2[i] = 0.;
    }
    const int no_trans = 0;
    const int trans    = 1;
    valib_matmat3_prod(no_trans, no_trans, Q,    A, aux1);
    valib_matmat3_prod(no_trans, trans,    aux1, Q, aux2);
    print_matrix3("Matrix QAQ^T by my function", aux2);

    //==========================================================================
    printf("\n");
    printf("Symmetric and skew-symmetric parts:\n");
    //==========================================================================
    valib_sym_antisym3(QAQT);
    print_matrix3("by library function (Omega is lower triangle, "
                  "S is the upper triangle including diagonal)", QAQT);

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            S[j*3+i]     = 0.5 * (aux2[j*3+i] + aux2[i*3+j]);
            Omega[j*3+i] = 0.5 * (aux2[j*3+i] - aux2[i*3+j]);
        }
    }
    print_matrix3("S by me", S);
    print_matrix3("Omega by me", Omega);

    //==========================================================================
    printf("\n");
    printf("Gram-Schmidt orthogonalization:\n");
    //==========================================================================
    VA_REAL U[3*3];
    valib_mat_copy3(A, U);
    print_matrix3("matrix before the Gram-Schmidt orthogonalization", U);

    valib_gram_schmidt3(U);
    print_matrix3("matrix after the Gram-Schmidt orthogonalization", U);

    VA_REAL UTU[3*3];
    valib_matmat3_prod(1, 0, U, U, UTU);
    VA_REAL identity[3*3];
    valib_mat_zero3(identity);
    identity[0] = 1.; identity[4] = 1.; identity[8] = 1.;
    check_matrix(UTU, identity);

    //==========================================================================
    printf("\n");
    printf("construction of matrix Q from the vector of average corotation\n");
    //==========================================================================
    VA_REAL Qswirl[3*3];
    valib_mat_zero3(Qswirl);
    valib_constructQfromN3(corotation, Qswirl);
    print_matrix3("Qswirl from the vector of average corotation", Qswirl);

    //==========================================================================
    printf("\n");
    printf("eigenvalue search\n");
    //==========================================================================
    VA_REAL a, b, c, d;
    a = 1.;
    valib_trace3(A, &b);
    b = -b;
    valib_second_invariant3(A, &c);
    valib_determinant3(A, &d);
    d = -d;
    // find the three complex roots
    int info;
    VA_COMPLEX x1, x2, x3;
    valib_solve_cubic_equation(a, b, c, d, &x1, &x2, &x3, &info);
    if (info == 0) {
       print_complex_scalar("x1", x1);
       print_complex_scalar("x2", x2);
       print_complex_scalar("x3", x3);
    }
    else {
       printf("Cannot find eigenvalues this way, info = %d.\n", info);
    }

    return 0;
}
