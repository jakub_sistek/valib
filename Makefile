# file with definitions of compilers etc.
include make.inc

INCDIR = include
INC += -I$(INCDIR)

OBJ = src/valib.o 

VALIB= lib/libvalib.a
TESTS= test/test_valib

LIBOTHERS = -lm

LIBS = -Llib -lvalib $(LIBOTHERS)

# rules
all: $(VALIB) $(TESTS)

# general rule for compiling C++ objects
.SUFFIXES:

%.o: %.c
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

# recompile library if headers change
$(OBJ) : $(INCDIR) 

# create the library
$(VALIB): $(OBJ)
	mkdir -p lib
	$(AR) $@ $^
	$(RANLIB) $@

# build tests
test/test_valib: \
   test/test_valib.o \
   $(VALIB)
	$(CC) $(CFLAGS) -o $@ $< $(LIBS)

# Fortran module
fortran: $(INCDIR)/valib_mod.o

$(INCDIR)/valib_mod.o: $(INCDIR)/valib_mod.F90
	cd $(INCDIR); $(FC) $(FFLAGS) -c $(notdir $<) -o $(notdir $@);

# generate doxygen documentation
doxy: 
	$(DOXYGEN) doc/doxy/Doxyfile;

# clean the directory
clean:
	rm -f $(VALIB) $(TESTS)
	rmdir lib
	cd src; rm -f *~ *.o;
	cd test; rm -f *~ *.o;
	cd include; rm -f *~ *.o *.mod;
	rm -f doc/doxy/errors.txt
