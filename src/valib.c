/**
 * @file
 *
 * Auxiliary file for generation of the precompiled library.
 * Functions are implemented in the included header files.
 **/

#include "contrarotation.h"
#include "corotation.h"
#include "delta.h"
#include "lambda2.h"
#include "qcriterion.h"
#include "qdcriterion.h"
#include "qmcriterion.h"
#include "qwcriterion.h"
#include "rortex.h"
#include "compactness.h"
#include "swirling_strength.h"
#include "triple_decomposition.h"
