/**
 * @file
 **/

#ifndef VA_QCRITERION_H
#define VA_QCRITERION_H

#include "qcriterion_common.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief \f$Q\f$-criterion
 * \cite Hunt-1988-ESC
 *
 * Vortices are identified as connected fluid regions with a positive second
 * invariant of the velocity-gradient tensor \f$ \nabla u \f$, which for an
 * incompressible flow simplifies to
 *  \f[
 *     Q = \frac{1}{2} \left( \| \Omega \|_F^2 - \| S \|_F^2 \right) > 0,
 *  \f]
 * where \f$ \Omega = \frac{1}{2} ( \nabla u - ( \nabla u )^T ) \f$  is the
 * antisymmetric part and
 * \f$ S = \frac{1}{2} ( \nabla u + ( \nabla u )^T ) \f$ is the symmetric part
 * of the velocity gradient \f$ \nabla u \f$. The criterion is fulfilled in the
 * regions where the vorticity magnitude prevails over the strain-rate
 * magnitude.
 *
 * Incompressibility is assumed in the derivation of this criterion,
 * i.e.
 *  \f[
 *     \mbox{trace}(\nabla u) = 0.
 *  \f]
 * For compressible flows, variants taking compressibility into account, i.e.
 * \f$Q_D\f$, \f$Q_M\f$, or \f$Q_W\f$, should be used.
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] qcriterion
 *             value of \f$ Q \f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_qcriterion(VA_REAL *A, VA_REAL *qcriterion)
{
    VA_REAL SO[9];    // symmetric (upper triangle) and antisymmetric
                      // (lower triangle) parts of velocity gradient

    // copy A to SO
    valib_mat_copy3(A, SO);

    // evaluate Q = 1/2 (||Omega||_F^2 - ||S||_F^2)
    valib_qcriterion_blending(SO, 1.0, qcriterion);
}

#endif // VA_QCRITERION_H
