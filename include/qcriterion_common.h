/**
 * @file
 **/

#ifndef VA_QCRITERION_COMMON_H
#define VA_QCRITERION_COMMON_H

#include "common_defs.h"
#include "linalg3.h"

/***************************************************************************//**
 * @name Helper functions for the Q-criterion and its variants.
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @ingroup valib_util
 *
 * Function common for \f$Q\f$, \f$Q_D\f$, \f$Q_W\f$, and \f$Q_M\f$
 * criterions with prescribed blending factor \f$\beta\f$ of the norms
 * \f$ \| \Omega \|_F^2 \f$ and \f$ \| S_X \|_F^2\f$.
 * Evaluates
 *  \f[
 *     Q = \frac{1}{2} ( \| \Omega \|_F^2 - \beta \| S_X \|_F^2 ) > 0,
 *  \f]
 * where \f$ \Omega = \frac{1}{2} ( \nabla u - ( \nabla u )^T ) \f$  is the
 * antisymmetric part and
 * \f$ S = \frac{1}{2} ( \nabla u + ( \nabla u )^T ) \f$ is the symmetric part
 * of the velocity gradient \f$ \nabla u \f$.
 *
 * For the \f$Q\f$-criterion \cite Hunt-1988-ESC
 *     \f$ S_X = S \f$ and \f$\beta = 1\f$.
 *
 * For the \f$Q_D\f$-criterion \cite Kolar-2009-CEV
 *     \f$ S_X = S_D \f$ and \f$\beta = 1\f$.
 *
 * For the \f$Q_M\f$-criterion \cite Kolar-2015-CCA
 *     \f$ S_X = S_D \f$ and \f$\beta = 1.5\f$.
 *
 * For the \f$Q_W\f$-criterion \cite Kolar-2019-VBB
 *     \f$ S_X = S_D \f$ and \f$\beta = 1.2\f$.
 *
 * Here, \f$ S_D \f$ is the deviatoric part of \f$ S \f$,
 * which is the same as the symmetric part of the deviatoric part of the
 * velocity gradient,
 *  \f[
 * ( \nabla u )_D = \nabla u - \frac{1}{3} \mbox{trace}(\nabla u)\ I.
 *  \f]
 ******************************************************************************/
inline VA_DEVICE_FUN void valib_qcriterion_blending(VA_REAL *SO,
                                                    VA_REAL blending_ratio,
                                                    VA_REAL *qcriterion_blended)
{
    // prepare symmetric and antisymmetric parts of SO
    valib_sym_antisym3(SO);

    // evaluate Q = 1/2 (||Omega||_F^2 - ||S||_F^2)
    *qcriterion_blended = 0.5
                        * (2.0*( SO[1]*SO[1]         // )
                                +SO[2]*SO[2]         // } Omega^2
                                +SO[5]*SO[5])        // )
                           - blending_ratio *
                             ( SO[0]*SO[0]           // )
                              +SO[4]*SO[4]           // } diagonal of S^2
                              +SO[8]*SO[8]           // )
                              +2.0*( SO[3]*SO[3]     // )
                                    + SO[6]*SO[6]    // } off-diagonal of S^2
                                    + SO[7]*SO[7])));// )
}

/***************************************************************************//**
 * @}
 ******************************************************************************/

#endif // VA_QCRITERION_COMMON_H
