/**
 * @file
 **/

#ifndef VA_QDCRITERION_H
#define VA_QDCRITERION_H

#include "qcriterion_common.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief \f$Q_D\f$-criterion
 * \cite Kolar-2009-CEV
 *
 * Vortices are identified as connected fluid regions with a positive quantity
 *  \f[
 *     Q_D = \frac{1}{2} \left( \| \Omega \|_F^2 - \| S_D \|_F^2 \right) > 0,
 *  \f]
 * where \f$ \Omega = \frac{1}{2} ( \nabla u - ( \nabla u )^T ) \f$, and
 * \f$ S_D = \frac{1}{2} ( (\nabla u)_D + (\nabla u )_D^T )\f$ is the symmetric
 * part of the deviatoric part of the velocity  gradient \f$ \nabla u \f$,
 *  \f[
 *    (\nabla u)_D = \nabla u - \frac{1}{3} \mbox{trace}(\nabla u)\ I.
 *  \f]
 * The criterion is a modification of the \f$Q\f$-criterion, which corrects
 * the effect of combressibility.
 * It is applicable also to compressible flows where
 *  \f[
 *     \mbox{trace}(\nabla u) \neq 0.
 *  \f]
 * An alternative might be using the \f$Q_M\f$ or \f$Q_W\f$ criterions, which
 * better suppress the effect of shear. This is important in regions with strong
 * `outer' shear, such as in boundary layers.
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] qd_criterion
 *             value of \f$ Q_D \f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_qdcriterion(VA_REAL *A, VA_REAL *qd_criterion)
{
    VA_REAL SO[9];    // symmetric (upper triangle) and antisymmetric
                      // (lower triangle) parts of velocity gradient

    // copy A to SO
    valib_mat_copy3(A, SO);

    // find the deviatoric part of A
    valib_deviatorise3(SO);

    // evaluate Q_D = 1/2 (||Omega||_F^2 - ||S_D||_F^2)
    valib_qcriterion_blending(SO, 1.0, qd_criterion);
}

#endif // VA_QDCRITERION_H
