/**
 * @file
 **/

#ifndef VA_DELTA_H
#define VA_DELTA_H

#include "common_defs.h"
#include "linalg3.h"

/***************************************************************************//**
 *  @ingroup valib_user
 *
 * \brief \f$ \Delta \f$-criterion
 * \cite Chong-1990-GCT \cite Dallmann-1983-TST \cite Vollmers-1983-SVF
 *
 *  Vortices are defined as the regions in which the velocity gradient
 *  \f$ \nabla u \f$ has a conjugate pair of complex eigenvalues.
 *  This corresponds to
 *  \f[
 *     \Delta = \left(\frac{Q}{3} \right)^3 + \left(\frac{R}{2}\right)^2 > 0,
 *  \f]
 *  where \f$ Q \f$ is the second invariant of \f$ \nabla u \f$, and \f$ R \f$
 *  is the third invariant (the determinant) of \f$ \nabla u \f$.
 *  Incompressibility is assumed in the derivation of this criterion,
 *  i.e.
 *  \f[
 *     \mbox{trace}(\nabla u) = 0.
 *  \f]
 *
 *  The criterion is derived from the discriminant of the characteristic
 *  polynomial
 *  \f[
 *     \lambda ^3 + Q\lambda - R = 0,
 *  \f]
 *  with \f$ \Delta  = 0 \f$ corresponding to the transition from three real
 *  eigenvalues to the case of one real eigenvalue and a conjugate pair of
 *  complex eigenvalues.
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] delta
 *             value of \f$ \Delta \f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_delta(VA_REAL *A, VA_REAL *delta)
{
    VA_REAL Q, R;

    // find second invariant
    valib_second_invariant3(A, &Q);

    // find third invariant (determinant)
    valib_determinant3(A, &R);

    // determine delta
    *delta = Q*Q*Q / (VA_REAL) 27.  + R*R / (VA_REAL) 4.;
}

#endif // VA_DELTA_H
