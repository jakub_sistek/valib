/**
 * @file
 *
 * Includes for the VALIB library functions.
 **/

#ifndef VA_VALIB_H
#define VA_VALIB_H

#include "common_defs.h"

VA_DEVICE_FUN void valib_contrarotation(VA_DEVICE_ADDR VA_REAL *A,
                                        VA_DEVICE_ADDR VA_REAL *S_RAVG);

VA_DEVICE_FUN void valib_corotation(VA_DEVICE_ADDR VA_REAL *A,
                                    VA_DEVICE_ADDR VA_REAL *omega_RAVG);

VA_DEVICE_FUN void valib_delta(VA_REAL *A, VA_REAL *delta);

VA_DEVICE_FUN void valib_lambda2(VA_REAL *A, VA_REAL *lambda_2);

VA_DEVICE_FUN void valib_qcriterion(VA_REAL *A, VA_REAL *qcriterion);

VA_DEVICE_FUN void valib_qdcriterion(VA_REAL *A, VA_REAL *qd_criterion);

VA_DEVICE_FUN void valib_qmcriterion(VA_REAL *A, VA_REAL *qm_criterion);

VA_DEVICE_FUN void valib_qwcriterion(VA_REAL *A, VA_REAL *qw_criterion);

VA_DEVICE_FUN void valib_rortex(VA_REAL *A, VA_REAL *rortex);

VA_DEVICE_FUN void valib_compactness(VA_REAL *A, VA_REAL *compactness);

VA_DEVICE_FUN void valib_swirling_strength(VA_REAL *A,
                                           VA_REAL *lambda_ci,
                                           VA_REAL *lambda_cr);

VA_DEVICE_FUN void valib_triple_decomposition(
    VA_DEVICE_ADDR VA_REAL *A,
    int *num_intervals,
    VA_DEVICE_ADDR VA_REAL *residual_vorticity,
    VA_DEVICE_ADDR VA_REAL *residual_strain,
    VA_DEVICE_ADDR VA_REAL *shear);

VA_DEVICE_FUN void valib_triple_decomposition_4norms(
    VA_DEVICE_ADDR VA_REAL *A, int *num_intervals,
    VA_DEVICE_ADDR VA_REAL *residual_vorticity,
    VA_DEVICE_ADDR VA_REAL *residual_strain,
    VA_DEVICE_ADDR VA_REAL *shear_vorticity,
    VA_DEVICE_ADDR VA_REAL *shear_strain);

VA_DEVICE_FUN void valib_triple_decomposition_with_angles(
    VA_DEVICE_ADDR VA_REAL *A,
    int *num_intervals,
    VA_DEVICE_ADDR VA_REAL *residual_vorticity,
    VA_DEVICE_ADDR VA_REAL *residual_strain,
    VA_DEVICE_ADDR VA_REAL *shear,
    VA_DEVICE_ADDR VA_REAL *aplha,
    VA_DEVICE_ADDR VA_REAL *beta,
    VA_DEVICE_ADDR VA_REAL *gamma
    );

#endif // VA_VALIB_H
