/**
 * @file
 **/

#ifndef VA_CONTRAROTATION_H
#define VA_CONTRAROTATION_H

#include "corotation_common.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * @brief Average contrarotation (a.k.a. \f$ S_{RAVG} \f$)
 * \cite Sistek-2017-ACC
 *
 * The symmetric tensor of average contrarotation at a point \f$x_0\f$ is
 * defined as a spherical average over a unit sphere \f$\sigma(0,1)\f$ as
 * \f[
 *    S_{RAVG}(x_0)
 *    = \beta\ \frac{\iint\limits_{\sigma(0,1)} S_{3,RES}(x_0, n)\ d\sigma}
 *                {\iint\limits_{\sigma(0,1)}\ d\sigma}
 *    = \frac{\beta}{4\pi}\iint\limits_{\sigma(0,1)}
 *      S_{3,RES}(x_0, n)\ d\sigma,
 * \f]
 * where \f$\beta\f$ is a scaling factor. The natural choice \f$\beta=5/2\f$
 * is derived in \cite Sistek-2017-ACC, and this value is used also here.
 * The tensor \f$ S_{3,RES}(x_0, n) \f$ is obtained by 2-D
 * analysis of the residual strain rate in a cutting plane given by a normal
 * \f$n\f$, and it corresponds to the contrarotation of line segments
 * near a point. Details can be found in \cite Sistek-2017-ACC .
 * It should not be confused with the \f$ S_{RES} \f$ tensor of the 3-D residual
 * strain rate from the Triple Decomposition Method \cite Kolar-2007-VIN.
 *
 * The average-contrarotation tensor can be seen as the strain-rate tensor
 * corrected by the effect of shear. As shown in \cite Sistek-2017-ACC, the
 * norm of the tensor \f$\| S_{RAVG} \|_F\f$ can be effectively used
 * for vizualising high strain-rate zones.
 *
 * Average contrarotation is determined from deviatoric quantities, thus it can
 * be applied to compressible flows for which
 *  \f[
 *     \mbox{trace}(\nabla u) \neq 0.
 *  \f]
 *
 *******************************************************************************
 *
 * @param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * @param[out] S_RAVG
 *             3x3 matrix with the tensor of averaged contrarotation, stored
 *             column-wise
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_contrarotation(VA_DEVICE_ADDR VA_REAL *A,
                                        VA_DEVICE_ADDR VA_REAL *S_RAVG)
{
    // initialize matrix of average contrarotation
    valib_mat_zero3(S_RAVG);

    // compute average contrarotation using Fibonacci integration
    valib_integrator_fibonacci(&valib_integrand_resstrain, A, S_RAVG);
}

#endif // VA_CONTRAROTATION_H
