/**
 * @file
 **/

#ifndef VA_QWCRITERION_H
#define VA_QWCRITERION_H

#include "qcriterion_common.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief \f$Q_W\f$-criterion
 * \cite Kolar-2019-VBB
 *
 * Vortices are identified as connected fluid regions with a positive quantity
 *  \f[
 *     Q_W = \frac{1}{2}
 *           \left( \| \Omega \|_F^2 - \frac{6}{5} \| S_D \|_F^2 \right) > 0,
 *  \f]
 * where \f$ \Omega = \frac{1}{2} ( \nabla u - ( \nabla u )^T ) \f$, and
 * \f$ S_D = \frac{1}{2} ( (\nabla u)_D + (\nabla u )_D^T )\f$ is the symmetric
 * part of the deviatoric part of the velocity  gradient \f$ \nabla u \f$,
 *  \f[
 *    (\nabla u)_D = \nabla u - \frac{1}{3} \mbox{trace}(\nabla u)\ I.
 *  \f]
 * The criterion is a modification of the \f$Q\f$-criterion, and it is based
 * on spherical averaging of the (planar) Okubo-Weiss criterion.
 * \f$Q_W\f$ corrects the effect of compressibility and eliminates the effect
 * of shear. Since it is derived from the deviatoric part of the velocity
 * gradient \f$\nabla u\f$, it is applicable also to compressible flows where
 *  \f[
 *     \mbox{trace}(\nabla u) \neq 0.
 *  \f]
 *
 * An alternative might be using the \f$Q_M\f$ criterion, which is stricter
 * than \f$Q_W\f$.
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] qw_criterion
 *             value of \f$ Q_W \f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_qwcriterion(VA_REAL *A, VA_REAL *qw_criterion)
{
    VA_REAL SO[9];    // symmetric (upper triangle) and antisymmetric
                      // (lower triangle) parts of velocity gradient

    // copy A to SO
    valib_mat_copy3(A, SO);

    // find the deviatoric part of A
    valib_deviatorise3(SO);

    // evaluate Q_M = 1/2 (||Omega||_F^2 - 6/5 ||S_D||_F^2)
    valib_qcriterion_blending(SO, 1.2, qw_criterion);
}

#endif // VA_QWCRITERION_H
