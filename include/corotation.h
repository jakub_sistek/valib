/**
 * @file
 **/

#ifndef VA_COROTATION_H
#define VA_COROTATION_H

#include "corotation_common.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief Average corotation (a.k.a. \f$\boldsymbol{\omega}_{RAVG}\f$)
 * \cite Kolar-2013-ACL
 *
 * The vector of average corotation at a point \f$x_0\f$ is defined
 * as a spherical average over a unit sphere \f$\sigma(0,1)\f$ as
 * \f[
 *    \boldsymbol{\omega}_{RAVG}(x_0) =
 *    3\ \frac{\iint\limits_{\sigma(0,1)}
 *             \boldsymbol{\omega}_{RES}(x_0, n)\ d\sigma}
 *            {\iint\limits_{\sigma(0,1)}\ d\sigma}
 *    = \frac{3}{4 \pi}\iint\limits_{\sigma(0,1)}
 *      \boldsymbol{\omega}_{RES}(x_0, n)\ d\sigma
 * \f]
 * The vector
 * \f[
 *    {\boldsymbol \omega}_{RES}(x_0, n) =
 *    2\omega_{RES}\ n
 * \f]
 * corresponds to the corotation of line-segments near a point in a cutting
 * plane given by a normal \f$n\f$.
 *
 * The length of the vector \f$ \omega_{RES} \f$ corresponds to the
 * residual vorticity (see \cite Kolar-2007-VIN) in the given 2-D plane
 * determined as
 * \f[
 *    \omega_{RES} = \left\{
 *    \begin{array}{ll}
 *    \omega - \omega_{SH} = (\mbox{sgn}\ \omega)( |\omega| - |s_D|)
 *                                         & \mbox{for}\ |\omega|\ge|s_D|, \\
 *    0                                    & \mbox{for}\ |\omega|\le|s_D|,
 *    \end{array}
 *    \right.
 * \f]
 * where
 * \f$ \omega_{SH} \f$ stands for the vorticity due to shear,
 * \f$ |s_D| \f$ is the planar deviatoric principal strain-rate magnitude
 * \f[
 *    |s_D|  = \left( \sqrt{(u_x - v_y)^2 + (u_y + v_x)^2}\right)/2,
 * \f]
 * and \f$\omega\f$ is the planar vorticity
 * \f[
 *    \omega = (v_x - u_y)/2.
 * \f]
 * These quantities are derived from a velocity gradient tensor in the 2-D plane
 * \f[\left(
 *   \begin{array}{cc}
 *     u_{x} & u_{y} \\
 *     v_{x} & v_{y}
 *   \end{array}
 *   \right)
 * \f]
 *
 * The average-corotation vector can be seen as a vector of vorticity corrected
 * by the effect of shear. Therefore, as shown in \cite Kolar-2013-ACL, the
 * magnitude of the vector \f$|\boldsymbol{\omega}_{RAVG}|\f$ can be
 * effectively used for vizualising vortices, unlike the magnitude of the
 * (uncorrected) vorticity vector \f$|\boldsymbol{\omega}|\f$,
 * \f$\boldsymbol{\omega} = \nabla \times u\f$, which cannot be effectively
 * used for vizualizing vortices in the presence of strong `outer' shear,
 * such as in boundary layers.
 *
 * Average corotation is determined from deviatoric quantities, thus it can
 * be applied to compressible flows for which
 *  \f[
 *     \mbox{trace}(\nabla u) \neq 0.
 *  \f]
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] omega_RAVG
 *             average-corotation vector of length 3
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_corotation(VA_DEVICE_ADDR VA_REAL *A,
                                    VA_DEVICE_ADDR VA_REAL *omega_RAVG)
{
    // initialize vector of average corotation
    valib_vec_zero3(omega_RAVG);

    // compute average corotation using the Fibonacci integration
    valib_integrator_fibonacci(&valib_integrand_resvort, A, omega_RAVG);
}

#endif // VA_COROTATION_H
