/**
 * @file
 **/

#ifndef VA_LAMBDA2_H
#define VA_LAMBDA2_H

#include "common_defs.h"
#include "linalg3.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief \f$ \lambda _2 \f$-criterion
 * \cite Jeong-1995-OIV
 *
 * Vortex is defined as a connected fluid region with two negative
 * eigenvalues of the symmetric tensor
 * \f[
 *    S^2 + \Omega^2,
 * \f]
 * where \f$ \Omega = \frac{1}{2} ( \nabla u - ( \nabla u )^T ) \f$ is the
 * antisymmetric part and
 * \f$ S = \frac{1}{2} ( \nabla u + (\nabla u )^T )\f$ is the symmetric
 * part of the velocity  gradient \f$ \nabla u \f$.
 *
 * In the underlying search for a local pressure minimum across a vortex,
 * the quantity \f$S^2 + \Omega^2\f$
 * is employed as an approximation of the pressure Hessian after removing the
 * unsteady irrotational straining and viscous effects from the strain-rate
 * transport equation for incompressible fluids.
 * The eigenvalues of \f$S^2 + \Omega^2\f$ are real and if they are ordered,
 * \f[
 *    \lambda_1 \geq \lambda_2 \geq \lambda_3,
 * \f]
 * the condition on two negative eigenvalues is equivalent to
 * \f[
 *    \lambda_2 < 0,
 * \f]
 * hence the name of the criterion.
 *
 * Incompressibility is assumed in the derivation of this criterion,
 * i.e.
 * \f[
 *    \mbox{trace}(\nabla u) = 0,
 * \f]
 * and the criterion should not be applied to flows with significant effects of
 * compressibility.
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] lambda_2
 *             the second eigenvalue of \f$S^2 + \Omega^2\f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_lambda2(VA_REAL *A, VA_REAL *lambda_2)
{
    VA_REAL SO[9];      // velocity gradient
    VA_REAL O[9];       // Omega
    VA_REAL SSOO[9];    // S*S + Omega*Omega
    VA_REAL eigSSOO[3]; // eigenvalues of SSOO

    // make a local copy of the velocity gradient
    valib_mat_copy3(A, SO);

    // find symmetric and antisymmetric parts
    valib_sym_antisym3(SO);

    // create full antisymmetric part and mirror
    // the lower part with the oposite sign
    O[1] = SO[1]; // (2,1)
    O[2] = SO[2]; // (3,1)
    O[5] = SO[5]; // (3,2)

    // antisymmetrize
    O[0] = 0.;    // (1,1)
    O[3] = -O[1]; // (1,2)
    O[4] = 0.;    // (2,2)
    O[6] = -O[2]; // (1,3)
    O[7] = -O[5]; // (2,3)
    O[8] = 0.;    // (3,3)

    // create the full symmetric part
    SO[1] = SO[3]; // (2,1)
    SO[2] = SO[6]; // (3,1)
    SO[5] = SO[7]; // (3,2)

    // zero resulting values together
    valib_mat_zero3(SSOO);
    // SSOO = S*S
    valib_matmat3_prod(0, 0, SO, SO, SSOO);
    // SSOO = SSOO + O*O
    valib_matmat3_prod(0, 0, O,  O,  SSOO);

    // find sorted eigenvalues
    valib_eigenvalues_sym3(SSOO, eigSSOO);

    // extract the second eigenvalue
    *lambda_2 = eigSSOO[1];
    //printf("eigenvalues: %lf, %lf, %lf \n", eigSSOO[0], eigSSOO[1], eigSSOO[2]);
}

#endif // VA_LAMBDA2_H
