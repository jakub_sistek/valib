/**
 * @file
 **/

#ifndef VA_SWIRLING_STRENGTH_H
#define VA_SWIRLING_STRENGTH_H

#include "common_defs.h"
#include "linalg3.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief Swirling strength criterion (a.k.a. \f$\lambda_{ci}\f$-criterion)
 * \cite Zhou-1999-MGC \cite Chakraborty-2005-ORB
 *
 * Vortex is defined as a region for which \f$ \nabla u \f$ has a pair
 * of complex (conjugate) eigenvalues,
 * \f[
 *    \lambda_{cr} \pm \lambda_{ci} i,
 * \f]
 * where \f$ \lambda_{cr} \f$ and \f$ \lambda_{ci} \f$ are the real and the
 * imaginary parts of the complex eigenvalue of \f$ \nabla u \f$, respectively.
 *
 * Absolute value of the imaginary part of these eigenvalues is used to
 * determine a vortex region in \cite Zhou-1999-MGC, i.e.
 * \f[
 *    \lambda_{ci} \ge \varepsilon.
 * \f]
 * In addition, according to \cite Chakraborty-2005-ORB, a vortex region needs
 * to satisfy
 * \f[
 *    -\kappa \le \lambda_{cr}/\lambda_{ci} \le \delta,
 * \f]
 *
 * It can be seen as a generalization of the \f$\Delta\f$-criterion, and these
 * two criteria are equivalent for zero thresholds, i.e.
 * \f[
 *    \Delta = 0 \Leftrightarrow \lambda_{ci} = 0.
 * \f]
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] lambda_ci
 *             absolute value of the imaginary part of the complex eigenvalue of
 *             \f$ \nabla u \f$, \f$ \lambda_{ci} \f$
 *
 * \param[out] lambda_cr
 *             the real part of the complex eigenvalue of \f$ \nabla u \f$,
 *             \f$ \lambda_{cr} \f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_swirling_strength(VA_REAL *A,
                                           VA_REAL *lambda_ci,
                                           VA_REAL *lambda_cr)
{
    VA_REAL Q, R, delta;

    // determine the discriminant of the cubic characteristic equation
    valib_delta(A, &delta);

    // find the second invariant
    valib_second_invariant3(A, &Q);

    // find the third invariant (determinant)
    valib_determinant3(A, &R);

    if (delta <= 0) {
        // only real eigenvalues exist
        *lambda_ci = 0.;
        *lambda_cr = 0.;
    }
    else {
        // one real eigenvalue and a pair of complex conjugate eigenvalues
        VA_REAL aux = sqrt(delta);

        VA_REAL aux2;
        aux2 = 0.5*R + aux;
        VA_REAL g1 = valib_sign(1., aux2) * pow(fabs( aux2 ), 1./3.);

        aux2 = 0.5*R - aux;
        VA_REAL g2 = valib_sign(1., aux2) * pow(fabs( aux2 ), 1./3.);

        *lambda_ci = (g1 - g2) * sqrt(3.) / 2.;
        *lambda_cr = -(g1 + g2) / 2.;
    }
}

#endif // VA_SWIRLING_STRENGTH_H
