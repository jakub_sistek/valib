/**
 * @file
 **/

#ifndef VA_QMCRITERION_H
#define VA_QMCRITERION_H

#include "qcriterion_common.h"

/***************************************************************************//**
 * @ingroup valib_user
 *
 * \brief \f$Q_M\f$-criterion
 * \cite Kolar-2015-CCA
 *
 * Acording to the `modified \f$Q\f$-criterion', vortices are identified as
 * connected fluid regions with a positive quantity
 *  \f[
 *     Q_M = \frac{1}{2}
 *           \left( \| \Omega \|_F^2 - \frac{3}{2} \| S_D \|_F^2 \right) > 0,
 *  \f]
 * where \f$ \Omega = \frac{1}{2} ( \nabla u - ( \nabla u )^T ) \f$, and
 * \f$ S_D = \frac{1}{2} ( (\nabla u)_D + (\nabla u )_D^T )\f$ is the symmetric
 * part of the deviatoric part of the velocity  gradient \f$ \nabla u \f$,
 *  \f[
 *    (\nabla u)_D = \nabla u - \frac{1}{3} \mbox{trace}(\nabla u)\ I.
 *  \f]
 * The criterion is a modification of the \f$Q\f$-criterion, and it is based on
 * corotation of line segments in principal planes of the strain-rate tensor.
 * \f$Q_M\f$ corrects the effect of compressibility and eliminates the effect of
 * shear. Since it is derived from the deviatoric part of the velocity gradient
 * \f$\nabla u\f$, it is applicable also to compressible flows where
 *  \f[
 *     \mbox{trace}(\nabla u) \neq 0.
 *  \f]
 * The criterial quantities \f$Q_M\f$, \f$Q_D\f$ and \f$Q\f$ can be related as
 *  \f[
 *     Q_M = Q + \frac{II_S}{2} = Q_D + \frac{II_{DS}}{2},
 *  \f]
 * where \f$ II_S \f$ and \f$ II_{DS} \f$ are the second invariants of
 * \f$ S \f$ and \f$ S_D \f$, respectively.
 * An alternative might be using the \f$Q_W\f$ criterion, which is less strict
 * than \f$Q_M\f$.
 *
 *******************************************************************************
 *
 * \param[in]  A
 *             3x3 matrix of velocity gradient stored column-wise
 *
 * \param[out] qm_criterion
 *             value of \f$ Q_M \f$
 *
 ******************************************************************************/
VA_DEVICE_FUN void valib_qmcriterion(VA_REAL *A, VA_REAL *qm_criterion)
{
    VA_REAL SO[9];    // symmetric (upper triangle) and antisymmetric
                      // (lower triangle) parts of velocity gradient

    // copy A to SO
    valib_mat_copy3(A, SO);

    // find the deviatoric part of A
    valib_deviatorise3(SO);

    // evaluate Q_M = 1/2 (||Omega||_F^2 - 3/2 ||S_D||_F^2)
    valib_qcriterion_blending(SO, 1.5, qm_criterion);
}

#endif // VA_QMCRITERION_H
